﻿using AOT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Assets {

    public class GWApi {
        public partial class NativeConstants {

            /// ERR_TIMEOUT_SOCKET_CONNECT -> 1000
            public const int ERR_TIMEOUT_SOCKET_CONNECT = 1000;

            /// ERR_BUF_RECV -> 1001
            public const int ERR_BUF_RECV = 1001;

            /// ERR_ILL_PARMS -> -2000
            public const int ERR_ILL_PARMS = -2000;

            /// ERR_BUF_TOO_SMALL -> -2001
            public const int ERR_BUF_TOO_SMALL = -2001;
        }
        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public struct Packet
        {

            /// int
            public int size;

            /// char[4096]
            [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 4096)]
            public byte[] data;
        }

        /// Return Type: int
        ///packet: Packet*
        //public delegate int handlePacket(ref Packet packet);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int handlePacket(System.IntPtr packet);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate int handleSocketClosed(int tag);


        [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct GWHeader {

            /// int
            public int size;

            /// int
            public int version;

            /// int
            public int seq;

            /// int
            public int fromID;

            /// int
            public int toID;

            /// int
            public int cmd;
        }

        [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct GWNullMsg {

            /// GWHeader
            public GWHeader header;
        }

        [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential)]
        public struct GWMsg {

            /// GWHeader
            public GWHeader header;
        }

        [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, CharSet = System.Runtime.InteropServices.CharSet.Ansi)]
        public struct GWLogin {

            /// GWHeader
            public GWHeader header;

            /// int
            public int userid;

            /// char[32]
            [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst = 32)]
            public string token;
        }

        public partial class NativeMethods {

            /// Return Type: void
            ///userID: int
            ///token: char*
            ///socketClient: void*
            [DllImport("umsp-client")]
            public static extern void login(int userID, [System.Runtime.InteropServices.InAttribute()] [System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string token, System.IntPtr socketClient);


            /// Return Type: void
            ///socketClient: void*
            [DllImport("umsp-client")]
            public static extern void heartbeat(System.IntPtr socketClient);


            /// Return Type: int
            ///buf: char*
            ///size: int
            ///socketClient: void*
            [DllImport("umsp-client")]
            public static extern int sendMsgToAll(byte[] pData, int size, System.IntPtr socketClient);

            [DllImport("umsp-client")]
            public static extern int sendJson(byte[] pData, int size, System.IntPtr socketClient);

            ///Return Type: int
            ///handler: handlePacket
            ///client: void*
            [DllImport("umsp-client")]
            public static extern int tryRecv(handlePacket handler, System.IntPtr client);

            ///Return Type: int tag
            ///handler: handlePacket
            ///client: void*
            [DllImport("umsp-client")]
            public static extern int tryRecvPacket(handlePacket handler);

            ///Return Type: int tag
            ///handler: handlePacket
            ///client: void*
            [DllImport("umsp-client")]
            public static extern int tryRecvJsonPacket(handlePacket handler);

            [DllImport("umsp-client")]
            public static extern int startLoop( System.IntPtr client, handlePacket handler,handleSocketClosed handler1);

            ///Return Type: void*
            ///host: char*
            ///port: int
            ///recvTimeout: int
            [DllImport("umsp-client")]
            public static extern System.IntPtr createTcpSocketClient(byte[] host, int hostSize, int port, int recvTimeout, int socketTag);
            //[System.Runtime.InteropServices.DllImportAttribute("umsp-client", EntryPoint = "createTcpSocketClient")]
            //public static extern System.IntPtr createTcpSocketClient([InAttribute()] [MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.LPStr)] string host, int hostSize, int port, int recvTimeout, int socketTag);


            /// Return Type: void
            ///client: void*
            [DllImport("umsp-client")]
            public static extern void destorySocketClient(System.IntPtr client);
            [DllImport("umsp-client")]
            public static extern void openLogTxt();
            [DllImport("umsp-client")]
            public static extern void closeLogTxt();


        }
    }
}
