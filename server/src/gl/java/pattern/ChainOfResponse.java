package gl.java.pattern;

import lombok.extern.slf4j.Slf4j;

import java.util.Vector;

/**
 * 责任链
 *
 * @param <T>
 * @author geliang
 */
@Slf4j
public abstract class ChainOfResponse<T> {
    public Vector<T> handlerList = new Vector<T>();

    public void addHandler(T handler) {
        if (handler == null) {
            log.warn(("handler is null"));
            return;
        }
        ;
        handlerList.add(handler);
    }

    public void removeHandler(T handler) {
        if (handler == null) {
            log.warn("handler is null");
            return;
        } ;
        handlerList.remove(handler);
    }
//	public abstract boolean handle();
}
