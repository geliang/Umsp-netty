package gl.java.umsp.room;

import gl.java.umsp.bean.JsonBean;
import gl.java.umsp.bean.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RoomUserChangeState extends JsonBean {

    public  String roomID;
    /**
     * enter or exit
     */
    public String userAction;
    public int userID;
    public List<User> currentUserList  = new ArrayList<User>();
    public RoomUserChangeState(int userID, String roomID, String action, Map<Integer, User> map){
        this.userAction = action;
        this.userID = userID;
        this.roomID = roomID;
        Set<Integer> integers = map.keySet();
        for (Integer integer : integers) {
            currentUserList.add(map.get(integer));
        }
    }
}
