package gl.java.umsp.room;

import gl.java.umsp.Umsp;
import gl.java.util.JsonUtil;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class GateWayProxyRoomMessageHandler extends RoomMessageDispatcher {

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        Umsp.returnStringMsg(Umsp.CMD_ROOM_SERVICE_LOGIN, JsonUtil.toString(RoomService.INSTANCE), ctx.channel());
    }

}