package gl.java.umsp.event;

public class KafkaClientTest {
    public static void main(String[] args) {

        EventSubscriber.registerAsyncSubscriberCallBack(new IEventSubscriber() {
            @Override
            public void onMessage(String channel, String key) {

            }
        }, EventServerConfig.TestChannel,"Hello");
        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            EventPublisher.pub(EventServerConfig.TestChannel, "hi :" + i);
            EventPublisher.pub("Hello", "hi :" + i);
        }
    }
}
