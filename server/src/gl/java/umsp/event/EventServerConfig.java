package gl.java.umsp.event;

import gl.java.umsp.UmspConfig;

public class EventServerConfig {
    public static String ServerUrl = UmspConfig.MessageQueueServerHost;
    public static final int ServerPort = 8989;
    public static final String TestChannel = "test";
}
