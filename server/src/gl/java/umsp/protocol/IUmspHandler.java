package gl.java.umsp.protocol;

import gl.java.umsp.UmspHeader;
import io.netty.channel.ChannelHandlerContext;

public interface IUmspHandler {
     boolean handle(ChannelHandlerContext ctx, UmspHeader msg);
}
