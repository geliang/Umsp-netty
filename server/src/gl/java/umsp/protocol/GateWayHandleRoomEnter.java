package gl.java.umsp.protocol;

import gl.java.umsp.ErrCode;
import gl.java.umsp.Umsp;
import gl.java.umsp.UmspConfig;
import gl.java.umsp.UmspHeader;
import gl.java.umsp.gateway.ProxyRoomServiceConnectionPool;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

public class GateWayHandleRoomEnter implements IUmspHandler {
    @Override
    public boolean handle(ChannelHandlerContext ctx, UmspHeader msg) {
        if (Umsp.CMD_ROOM_ENTER == msg.cmd){
            Channel connectionChannel = ProxyRoomServiceConnectionPool.getInstance().getConnectionChannel(msg.gameID + UmspConfig.LINK_CHAR_KEY+msg.serviceID);
            if (connectionChannel == null) {
                Umsp.returnErrMsg(Umsp.CMD_ERR, ErrCode.RoomSerViceOffline,ctx.channel());
            }else{
                Umsp.returnMsg(msg, connectionChannel);
            }
            return true;
        }
        return false;
    }
}
