package gl.java.umsp.gateway;

import gl.java.umsp.room.RoomService;
import gl.java.umsp.room.RoomServiceConfig;
import gl.java.util.ConnectionPool;

public class ProxyRoomServiceConnectionPool extends ConnectionPool<String, RoomServiceConfig> {

    private static class SinglonHolder {
        final static ProxyRoomServiceConnectionPool holder = new ProxyRoomServiceConnectionPool();
    }

    public static ProxyRoomServiceConnectionPool getInstance() {
        return SinglonHolder.holder;
    }
}
