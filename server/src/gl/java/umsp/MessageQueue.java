package gl.java.umsp;

import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.PriorityBlockingQueue;
@Slf4j
public class MessageQueue<T> {
	private PriorityBlockingQueue<T> queue = new PriorityBlockingQueue<T>();
	private static class SinglonHolder {
		 @SuppressWarnings("rawtypes")
        final static  MessageQueue<?> holder = new MessageQueue();
	}
	public static MessageQueue<?> getInstance(){
		return SinglonHolder.holder;
	}
	public void push(T usmpFrame) {
        log.info("push"+usmpFrame.toString());
		queue.put(usmpFrame);
	}

}
