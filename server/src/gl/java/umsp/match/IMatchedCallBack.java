package gl.java.umsp.match;

import gl.java.umsp.bean.MatchResult;

public interface IMatchedCallBack {
    public void onMatched(MatchResult result);
}
