package gl.java.util;

import java.util.concurrent.TimeUnit;

public class TimeUtil {
   public static long  currentTimeMillis() {
       return  TimeUnit.MILLISECONDS.convert(System.nanoTime(),TimeUnit.NANOSECONDS);
    }
}
