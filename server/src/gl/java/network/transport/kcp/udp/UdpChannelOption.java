package gl.java.network.transport.kcp.udp;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import io.netty.channel.ChannelException;
import io.netty.channel.ChannelOption;

import java.io.IOException;
import java.net.SocketOption;
import java.net.StandardSocketOptions;
import java.nio.channels.Channel;
import java.nio.channels.NetworkChannel;
import java.nio.channels.ServerSocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class UdpChannelOption<T> extends ChannelOption<T> {
    private final SocketOption<T> option;

    private UdpChannelOption(SocketOption<T> option) {
        super(option.name());
        this.option = option;
    }

    public static <T> ChannelOption<T> of(SocketOption<T> option) {
        return new UdpChannelOption(option);
    }

    static <T> boolean setOption(Channel jdkChannel, UdpChannelOption<T> option, T value) {
        NetworkChannel channel = (NetworkChannel)jdkChannel;
        if (!channel.supportedOptions().contains(option.option)) {
            return false;
        } else if (channel instanceof ServerSocketChannel && option.option == StandardSocketOptions.IP_TOS) {
            return false;
        } else {
            try {
                channel.setOption(option.option, value);
                return true;
            } catch (IOException var5) {
                throw new ChannelException(var5);
            }
        }
    }

    static <T> T getOption(Channel jdkChannel, UdpChannelOption<T> option) {
        NetworkChannel channel = (NetworkChannel)jdkChannel;
        if (!channel.supportedOptions().contains(option.option)) {
            return null;
        } else if (channel instanceof ServerSocketChannel && option.option == StandardSocketOptions.IP_TOS) {
            return null;
        } else {
            try {
                return channel.getOption(option.option);
            } catch (IOException var4) {
                throw new ChannelException(var4);
            }
        }
    }

    static ChannelOption[] getOptions(Channel jdkChannel) {
        NetworkChannel channel = (NetworkChannel)jdkChannel;
        Set<SocketOption<?>> supportedOpts = channel.supportedOptions();
        if (channel instanceof ServerSocketChannel) {
            List<ChannelOption<?>> extraOpts = new ArrayList(supportedOpts.size());
            Iterator var8 = supportedOpts.iterator();

            while(var8.hasNext()) {
                SocketOption<?> opt = (SocketOption)var8.next();
                if (opt != StandardSocketOptions.IP_TOS) {
                    extraOpts.add(new UdpChannelOption(opt));
                }
            }

            return (ChannelOption[])extraOpts.toArray(new ChannelOption[0]);
        } else {
            ChannelOption<?>[] extraOpts = new ChannelOption[supportedOpts.size()];
            int i = 0;

            SocketOption opt;
            for(Iterator var5 = supportedOpts.iterator(); var5.hasNext(); extraOpts[i++] = new UdpChannelOption(opt)) {
                opt = (SocketOption)var5.next();
            }

            return extraOpts;
        }
    }
}
