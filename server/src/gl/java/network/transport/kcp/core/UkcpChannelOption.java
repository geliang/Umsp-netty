package gl.java.network.transport.kcp.core;

import io.netty.channel.ChannelOption;

/**

 */
public final class UkcpChannelOption<T> extends ChannelOption<T> {

    public static final ChannelOption<Boolean> UKCP_NODELAY =
            valueOf(UkcpChannelOption.class, "UKCP_NODELAY");

    public static final ChannelOption<Integer> UKCP_INTERVAL =
            valueOf(UkcpChannelOption.class, "UKCP_INTERVAL");

    public static final ChannelOption<Integer> UKCP_FAST_RESEND =
            valueOf(UkcpChannelOption.class, "UKCP_FAST_RESEND");

    public static final ChannelOption<Boolean> UKCP_NOCWND =
            valueOf(UkcpChannelOption.class, "UKCP_NOCWND");

    public static final ChannelOption<Integer> UKCP_MIN_RTO =
            valueOf(UkcpChannelOption.class, "UKCP_MIN_RTO");
    /**
     * MTU，数据链路层规定的每一帧的最大长度，超过这个长度数据会被分片。
     * 通常MTU的长度为1500字节，IP协议规定所有的路由器均应该能够转发（512数据+60IP首部+4预留=576字节）的数据。
     * MSS，最大输出大小（双方的约定），KCP的大小为MTU-kcp头24字节。IP数据报越短，路由器转发越快，但是资源利用率越低。
     * 传输链路上的所有MTU都一至的情况下效率最高，应该尽可能的避免数据传输的工程中，再次被分。UDP再次被分的后（通常1分为2），只要丢失其中的任意一份，两份都要重新传输。
     * 因此，合理的MTU应该是保证数据不被再分的前提下，尽可能的大。
     * 以太网的MTU通常为1500字节-IP头（20字节固定+40字节可选）-UDP头8个字节=1472字节。
     * KCP会考虑多传输协议，但是在UDP的情况下，设置为1472字节更为合理。
     */
    public static final ChannelOption<Integer> UKCP_MTU =
            valueOf(UkcpChannelOption.class, "UKCP_MTU");

    public static final ChannelOption<Integer> UKCP_RCV_WND =
            valueOf(UkcpChannelOption.class, "UKCP_RCV_WND");

    public static final ChannelOption<Integer> UKCP_SND_WND =
            valueOf(UkcpChannelOption.class, "UKCP_SND_WND");

    public static final ChannelOption<Boolean> UKCP_STREAM =
            valueOf(UkcpChannelOption.class, "UKCP_STREAM");

    public static final ChannelOption<Integer> UKCP_DEAD_LINK =
            valueOf(UkcpChannelOption.class, "UKCP_DEAD_LINK");

    public static final ChannelOption<Boolean> UKCP_AUTO_SET_CONV =
            valueOf(UkcpChannelOption.class, "UKCP_AUTO_SET_CONV");

    public static final ChannelOption<Boolean> UKCP_FAST_FLUSH =
            valueOf(UkcpChannelOption.class, "UKCP_FAST_FLUSH");

    public static final ChannelOption<Boolean> UKCP_MERGE_SEGMENT_BUF =
            valueOf(UkcpChannelOption.class, "UKCP_MERGE_SEGMENT_BUF");

    @SuppressWarnings("deprecation")
    private UkcpChannelOption() {
        super(null);
    }

}
