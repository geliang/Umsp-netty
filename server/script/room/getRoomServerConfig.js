function getRoomServerConfig() {
    return JSON.stringify(
        {
            gameID: 0,
            IP: "127.0.0.1",
            port: 8899,
            portWebSocket: 8898,
            serverIndex: 1,
            desKey: "des-room"
        }
    );
}