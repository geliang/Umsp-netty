var scopeVar = 1;
var global = this;
undefGlobal = this;

function scopeTest() {
    if (this !== global) {
        throw new Error("this !== global");
    }
    if (this !== undefGlobal) {
        throw new Error("this !== undefinedGlobal")
    }
    return scopeVar;
}

scopeTest();