/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : glmall

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-07-21 00:56:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for activity
-- ----------------------------
DROP TABLE IF EXISTS `activity`;
CREATE TABLE `activity` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) DEFAULT NULL,
  `bannerurl` varchar(1024) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `isvalue` int(32) unsigned zerofill DEFAULT NULL,
  `contexturl` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of activity
-- ----------------------------
INSERT INTO `activity` VALUES ('1', '注册有礼', '/img/acitivity/activity1.jpg', '2016-05-31 22:36:42', '2016-06-01 22:36:46', '00000000000000000000000000000000', '/activity/1.html');
INSERT INTO `activity` VALUES ('2', '推荐有礼', '/img/acitivity/activity2.jpg', '2016-05-31 22:38:53', '2016-05-28 22:38:57', '00000000000000000000000000000000', '/activity/2.html');

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('1', 'JFinal Demo Title here', 'JFinal Demo Content here');
INSERT INTO `blog` VALUES ('2', 'test 1', 'test 1');
INSERT INTO `blog` VALUES ('3', 'test 2', 'test 2');
INSERT INTO `blog` VALUES ('4', 'test 3', 'test 3');
INSERT INTO `blog` VALUES ('5', 'test 5', 'test 4');
INSERT INTO `blog` VALUES ('6', 'sfs', 'sfdf');

-- ----------------------------
-- Table structure for calendar
-- ----------------------------
DROP TABLE IF EXISTS `calendar`;
CREATE TABLE `calendar` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `data` datetime DEFAULT NULL,
  `fgoodsid` int(32) DEFAULT NULL,
  `fgoodstype` int(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `goodsid` (`fgoodsid`),
  KEY `calendar_ibfk_1` (`fgoodstype`),
  CONSTRAINT `calendar_ibfk_1` FOREIGN KEY (`fgoodstype`) REFERENCES `goodstype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `goodsid` FOREIGN KEY (`fgoodsid`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='水果日历推荐商品';

-- ----------------------------
-- Records of calendar
-- ----------------------------
INSERT INTO `calendar` VALUES ('4', '2016-06-19 00:00:00', '6', '8');
INSERT INTO `calendar` VALUES ('5', '2016-06-20 00:00:00', '7', '8');
INSERT INTO `calendar` VALUES ('6', '2016-06-24 00:00:00', '8', '8');
INSERT INTO `calendar` VALUES ('7', '2016-07-01 00:00:00', '9', '9');
INSERT INTO `calendar` VALUES ('8', '2016-08-02 00:00:00', '10', '9');
INSERT INTO `calendar` VALUES ('9', '2016-09-01 00:00:00', '11', '9');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `forderid` int(32) DEFAULT NULL,
  `fuserid` int(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `isused` int(1) unsigned zerofill DEFAULT NULL,
  `limit` int(32) unsigned zerofill DEFAULT NULL,
  `value` int(32) unsigned zerofill DEFAULT NULL,
  `name` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderuserid` (`fuserid`),
  KEY `forderid` (`forderid`),
  CONSTRAINT `coupon_ibfk_1` FOREIGN KEY (`forderid`) REFERENCES `userorder` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `coupon_ibfk_2` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('1', null, '18', '2016-05-31 23:37:54', '2016-05-30 23:37:59', '1', '00000000000000000000000000000000', '00000000000000000000000000000015', '满100减5');

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `fgoodstype` int(32) NOT NULL,
  `price` int(32) NOT NULL,
  `discount` int(32) unsigned zerofill DEFAULT NULL,
  `contexturl` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `count` int(32) DEFAULT NULL,
  `countremaining` int(32) DEFAULT NULL,
  `limitbuycount` int(32) DEFAULT NULL COMMENT '单个用户的购买数量',
  `fplaceid` int(32) DEFAULT NULL,
  `subtitle` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `mass` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `isalive` int(32) DEFAULT NULL,
  `img` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `context` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fgoodstype` (`fgoodstype`),
  KEY `fplaceid` (`fplaceid`),
  CONSTRAINT `fgoodstype` FOREIGN KEY (`fgoodstype`) REFERENCES `goodstype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fplaceid` FOREIGN KEY (`fplaceid`) REFERENCES `place` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of goods
-- ----------------------------
INSERT INTO `goods` VALUES ('1', '樱桃', '5', '100', '00000000000000000000000000000001', 'dboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('2', '樱桃1', '6', '200', '00000000000000000000000000000001', 'dboomsky.comdboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('3', '3樱桃', '6', '300', '00000000000000000000000000000001', 'dboomsky.comdboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('4', '大个樱桃', '7', '14', '00000000000000000000000000000001', 'dboomsky.comdboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('5', '大个樱桃', '7', '155', '00000000000000000000000000000001', 'dboomsky.comdboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('6', '1个樱桃', '8', '300', '00000000000000000000000000000001', 'dboomsky.comdboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('7', '1个苹果', '8', '200', '00000000000000000000000000000001', 'dboomsky.comdboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('8', '1根香蕉', '8', '100', '00000000000000000000000000000001', 'dboomsky.comdboomsky.com', '3', '2', '1', '1', '(来自新西兰的樱桃)', '20个(1KG起', '0', '/img/defaultavatar.jpg', '没什么好介绍的,就是一颗晚上11点吃了会有幸福感的樱桃.文字总数不得超过1024个');
INSERT INTO `goods` VALUES ('9', '水蜜桃', '8', '400', '00000000000000000000000000000001', '', '0', '0', '0', '1', '鲜嫩爽口', '6个装      1.2kg起', '0', '/img/goods/shuimitao.jpg', '直接从产地新鲜采摘来的，经过精挑细选，最后我们看到、吃到的是汁多味甜，粉嫩诱人的蜜桃，酸甜可口的味道，会让你无限回味。');
INSERT INTO `goods` VALUES ('10', '1根黄瓜', '8', '100', '00000000000000000000000000000001', null, '0', '0', '0', '1', '123', '1', '1', '/img/defaultavatar.jpg', '132');
INSERT INTO `goods` VALUES ('11', '烟台水晶富士苹果', '0', '24000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '口感清甜，美味营养', '6枚装      1.2kg起', '0', '/img/goods/apple1.jpg', '种植方式的生态、有机，含有更为丰富的果糖、维生素和大脑所必须的营养素，不仅能够增强记忆力，还能够润肤美白，延缓衰老，瘦身美容。');
INSERT INTO `goods` VALUES ('12', '甘肃特级红富士苹果', '0', '28000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '紧致酥脆，清香诱人', '4枚装      1.0kg起', '0', '/img/goods/apple2.jpg', '瘦身美容，滋养皮肤！瘦身美容，滋养皮肤！瘦身美容，滋养皮肤！重要的事情要说三遍。当然，你知道的，苹果还具有调理肠胃，促进肾机能的功效。');
INSERT INTO `goods` VALUES ('13', '海南乐东火龙果', '0', '12000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '雪肤花貌，清甜多汁', '2个装      0.6kg起', '0', '/img/goods/huolongguo.jpg', '预防贫血、降低胆固醇、美白皮肤防黑斑，果肉中的芝麻状种子还具有促进肠胃消化功能，关键还有味道好极了~~');
INSERT INTO `goods` VALUES ('14', '皇冠蜜梨', '0', '28000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '梨不开的味道', '6个装      1.2kg起', '0', '/img/goods/mili.jpg', '百果之宗，清热佳品，小心翼翼张开嘴，只听一声脆响，清香甘甜沁心脾，啊哈，水润柔嫩，清香甘甜。');
INSERT INTO `goods` VALUES ('15', '海南儋州香蕉', '0', '24000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '香甜软糯，四季皆宜', '2kg', '0', '/img/goods/xiangjiao.jpg', '褪去外衣便可大口享用，浓郁的蕉香四溢，醇甜的滋味入口，经常食用，可以保持一个好气色哦！');
INSERT INTO `goods` VALUES ('16', '进口红提', '0', '32000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '一颗一颗，晶莹剔透', '1kg', '0', '/img/goods/hongti.jpg', '果实呈深红色，果肉硬脆，刀切而不留汁，味甜可口，风味纯正，营养丰富，重点介绍，它可是补血固肾的冠军哟。');
INSERT INTO `goods` VALUES ('17', '新疆哈密瓜', '0', '50000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '西域风情', '1个装      1.5kg起', '0', '/img/goods/hamigua.jpg', '咔嚓一口，水滋滋的甜，蜜瓜瓤厚细腻，越靠近中心处，甜度越高，口口松脆甘爽，天山雪水灌溉出来的瓜就是不一样~~');
INSERT INTO `goods` VALUES ('18', '赣南脐橙', '0', '30000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '刚刚被太阳亲吻过', '6个装      1.2kg起', '0', '/img/goods/qicheng.jpg', '丰富的维C，酸甜的味道，光是想想就不要不要了，再加上它还有补血和增强抵抗力的效果，哈哈~~');
INSERT INTO `goods` VALUES ('19', '新西兰奇异果', '0', '36000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '国王登场', '4个装      0.6kg起', '0', '/img/goods/qiyiguo.jpg', '维他命C及钙含量卫冕水果王国榜首，是美白的极品，同时对提升睡眠品质有很大的帮助，想要提升睡眠质量的宝宝可以每天来一颗哦~~');
INSERT INTO `goods` VALUES ('20', '台湾凤梨', '0', '55000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '甜甜甜~~~', '1个装      1.0kg起', '0', '/img/goods/fengli.jpg', '感谢台湾的热带季风气候，得天独厚的自然生态条件，为我们带来了嫩甜的口感享受，哼哼，前方高能•••••食肉及油腻食物后，吃些凤梨可预防脂肪沉积。');
INSERT INTO `goods` VALUES ('21', '山东烟台樱桃', '0', '60000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '果中钻石', '1.0kg', '0', '/img/goods/yingtao.jpg', '烟台樱桃成熟较早，果实色泽鲜艳，晶莹美丽、红如玛瑙，黄如凝脂，那味道，啧啧~~讲真，据说樱桃和女神更配哦。');
INSERT INTO `goods` VALUES ('22', '福建红肉蜜柚', '0', '15000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '肉粒可以数着吃~~', '1个装      1.0kg起', '0', '/img/goods/miyou.jpg', '丰富的果胶可以降低胆固醇，维生素P可以增加血管弹性，常吃蜜柚，可以帮助润肺哦。');
INSERT INTO `goods` VALUES ('23', '泰国金枕榴莲', '0', '110000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '又爱又恨的泰式滋味', '1个装      2.5kg起', '0', '/img/goods/liulian.jpg', '作为泰国榴莲的明星代表，金枕头是榴莲爱好者的心头好，浓郁而鲜甜的嫩滑，让味蕾迷上这美丽的冲动。');
INSERT INTO `goods` VALUES ('24', '海南麒麟西瓜', '0', '32000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '夏天的味道', '1个装      4kg起', '0', '/img/goods/xigua.jpg', '薄脆的瓜皮，红艳的瓜瓤，只要咔嚓一口，爽脆的瓜瓤应声崩开，甘甜的汁水喷涌而出，哈哈，简直无与伦比的满足，还有，西瓜寒凉，体热的人可以多吃哦。');
INSERT INTO `goods` VALUES ('25', '广西横县木瓜', '0', '20000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '传说中的万寿果', '2个装      1kg起', '0', '/img/goods/mugua.jpg', '木瓜中含有丰富的蛋白酵素，可以帮助分解蛋白质和淀粉，对消化系统很有帮助，而且香甜软糯的味道也是让人不要不要呢。');
INSERT INTO `goods` VALUES ('26', '越南芒果', '0', '32000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '小黄人一枚', '1kg', '0', '/img/goods/mangguo.jpg', '个头小巧饱满，金灿灿的表皮，软糯的口感，丰厚的果肉，色香味俱全，大大享受，舌尖的享受尽情迷恋吧。');
INSERT INTO `goods` VALUES ('27', '陕西渭南圣女果', '0', '18000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '一颗一颗好味道', '1kg', '0', '/img/goods/shengnvguo.jpg', '果实饱满，红色莹润，咬一口酸甜的味道，回味无穷，怎么能忘怀，就是这么好吃，就是这么美好。');
INSERT INTO `goods` VALUES ('28', '精选蓝莓', '0', '55000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '这里有蓝精灵', '0.5kg', '0', '/img/goods/lanmei.jpg', '外层白白的朦胧果粉是蓝莓的保护色，一大半营养可都在果粉里啊，花青素含量尤为丰富哦，想想，一把蓝莓丢进口中，瞬间爆浆又细腻的幸福感，是不是很嗨皮呢。');
INSERT INTO `goods` VALUES ('29', '墨西哥牛油果', '0', '60000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '营养多到不想说', '4个装      1kg起', '0', '/img/goods/niuyouguo.jpg', '外表坚硬，可内心是柔软的，绵如乳络般的丝滑，齿颊间回转淡淡的油脂香气，但是丝毫不会腻的。');
INSERT INTO `goods` VALUES ('30', '无锡水蜜桃', '0', '540000', '00000000000000000000000000000001', null, '0', '0', '0', '1', '鲜嫩爽口', '6个装      1.2kg起', '0', '/img/goods/shuimitao.jpg', '直接从产地新鲜采摘来的，经过精挑细选，最后我们看到、吃到的是汁多味甜，粉嫩诱人的蜜桃，酸甜可口的味道，会让你无限回味。');

-- ----------------------------
-- Table structure for goodstype
-- ----------------------------
DROP TABLE IF EXISTS `goodstype`;
CREATE TABLE `goodstype` (
  `id` int(32) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `showlevel` int(11) DEFAULT NULL COMMENT '显示的等级,0为普通分类,1为首页推荐分类.2为水果日历分类,',
  `icon` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='showlevel \r\n0 普通分类上平\r\n1 首页推荐商品\r\n2 水果日历商品\r\n\r\nicon\r\n分类图标\r\n\r\nname \r\n分类名称\r\n';

-- ----------------------------
-- Records of goodstype
-- ----------------------------
INSERT INTO `goodstype` VALUES ('0', '鲜果', '0', '');
INSERT INTO `goodstype` VALUES ('1', '果饮', '0', null);
INSERT INTO `goodstype` VALUES ('2', '干果', '0', null);
INSERT INTO `goodstype` VALUES ('3', '下午茶', '0', null);
INSERT INTO `goodstype` VALUES ('4', '周末套餐', '0', null);
INSERT INTO `goodstype` VALUES ('5', '新品预购', '1', '/img/typenew.png');
INSERT INTO `goodstype` VALUES ('6', '热销商品', '1', '/img/typehot.png');
INSERT INTO `goodstype` VALUES ('7', '人气推荐', '1', '/img/typestar.png');
INSERT INTO `goodstype` VALUES ('8', '热带风情', '2', null);
INSERT INTO `goodstype` VALUES ('9', '江南水诱', '2', null);

-- ----------------------------
-- Table structure for leavemessage
-- ----------------------------
DROP TABLE IF EXISTS `leavemessage`;
CREATE TABLE `leavemessage` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `context` varchar(4096) NOT NULL DEFAULT '',
  `createtime` bigint(32) DEFAULT NULL,
  `likecount` int(32) DEFAULT NULL,
  `userid` int(32) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `useravator` varchar(1024) DEFAULT NULL,
  `isanonymous` int(32) DEFAULT NULL,
  `isdelete` int(32) DEFAULT NULL,
  `isreplay` int(32) DEFAULT NULL,
  `replaycount` int(32) DEFAULT NULL,
  `replaymessageid` int(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of leavemessage
-- ----------------------------
INSERT INTO `leavemessage` VALUES ('20', '吾有上将潘凤', '1464192337514', '0', '18', '赵云', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '1', '0', '0', '4', '0');
INSERT INTO `leavemessage` VALUES ('21', '吾有上将潘凤', '1464192347217', '4', '18', '赵云', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '1', '0', '0', '4', '0');
INSERT INTO `leavemessage` VALUES ('22', '可斩华雄', '1464192527913', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '21');
INSERT INTO `leavemessage` VALUES ('23', '可斩华雄replay 赵云 可斩华... \n', '1464192584135', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '21');
INSERT INTO `leavemessage` VALUES ('24', 'replay 赵云 吾有上将潘... \n', '1464192686646', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '21');
INSERT INTO `leavemessage` VALUES ('25', 'replay 赵云 吾有上将潘... \n可斩华雄', '1464192741598', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '21');
INSERT INTO `leavemessage` VALUES ('26', '吾有上将潘凤', '1464193002774', '5', '18', '赵云', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '1', '0', '0', '0', '0');
INSERT INTO `leavemessage` VALUES ('27', '吾有上将潘凤', '1464516105653', '0', '18', '赵云', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '1', '0', '0', '1', '0');
INSERT INTO `leavemessage` VALUES ('28', 'replay 赵云 吾有上将潘... \n可斩华雄', '1464516130003', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '27');
INSERT INTO `leavemessage` VALUES ('29', 'replay 赵云 吾有上将潘... \n可斩华雄', '1466267798321', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '20');
INSERT INTO `leavemessage` VALUES ('30', 'replay 赵云 吾有上将潘... \n可斩华雄', '1466268022296', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '20');
INSERT INTO `leavemessage` VALUES ('31', 'replay 赵云 吾有上将潘... \n可斩华雄', '1466268171906', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '20');
INSERT INTO `leavemessage` VALUES ('32', 'replay 赵云 吾有上将潘... \n可斩华雄', '1466318315683', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '20');
INSERT INTO `leavemessage` VALUES ('33', '吾有上将潘凤', '1466437515192', '0', '18', '赵云', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '1', '0', '0', '3', '0');
INSERT INTO `leavemessage` VALUES ('34', 'replay 赵云 吾有上将潘... \n可斩华雄', '1466437637675', '0', '18', '曹操', 'http://img5.imgtn.bdimg.com/it/u=185842654,3990106999', '0', '0', '1', '0', '33');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `fuserid` int(32) DEFAULT NULL,
  `toid` int(32) DEFAULT NULL,
  `fromid` int(32) DEFAULT NULL,
  `summary` varchar(1024) NOT NULL,
  `createtime` bigint(64) unsigned zerofill DEFAULT NULL,
  `isread` int(10) DEFAULT NULL,
  `type` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuserid` (`fuserid`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('1', '18', '0', '0', 'xx大厦1-101', '0000000000000000000000000000000000000000000000000000000000000000', '1', '0');
INSERT INTO `message` VALUES ('3', '18', '0', '0', '系统消息1', '0000000000000000000000000000000000000000000000000000000000000000', '0', '0');
INSERT INTO `message` VALUES ('4', '18', '0', '0', '公告', '0000000000000000000000000000000000000000000000000000000000000000', '0', '0');
INSERT INTO `message` VALUES ('5', '18', '16', '0', '新的订单:16', '0000000000000000000000000000000000000000000000000001466267725639', '0', '1');
INSERT INTO `message` VALUES ('7', '18', '31', '18', '有人回复了您的留言', '0000000000000000000000000000000000000000000000000001466268171923', '0', '2');
INSERT INTO `message` VALUES ('8', '18', '18', '0', '系统消息', '0000000000000000000000000000000000000000000000000001466268171923', '0', '0');
INSERT INTO `message` VALUES ('9', '18', '32', '18', '有人回复了您的留言', '0000000000000000000000000000000000000000000000000001466318315703', '0', '2');
INSERT INTO `message` VALUES ('10', '18', '21', '18', '有人赞了您的留言', '0000000000000000000000000000000000000000000000000001466318347859', '0', '2');
INSERT INTO `message` VALUES ('11', '18', '33', '18', '有人回复了您的留言', '0000000000000000000000000000000000000000000000000001466437642349', '0', '2');
INSERT INTO `message` VALUES ('12', '18', '17', '0', '新的订单:17', '0000000000000000000000000000000000000000000000000001466439593081', '0', '1');
INSERT INTO `message` VALUES ('13', '18', '18', '0', '新的订单:18', '0000000000000000000000000000000000000000000000000001466440622591', '0', '1');
INSERT INTO `message` VALUES ('14', '18', '19', '0', '新的订单:19', '0000000000000000000000000000000000000000000000000001466440674498', '0', '1');
INSERT INTO `message` VALUES ('15', '18', '20', '0', '新的订单:20', '0000000000000000000000000000000000000000000000000001466440743505', '0', '1');
INSERT INTO `message` VALUES ('16', '18', '21', '0', '新的订单:21', '0000000000000000000000000000000000000000000000000001467537055006', '0', '1');
INSERT INTO `message` VALUES ('17', '18', '22', '0', '新的订单:22', '0000000000000000000000000000000000000000000000000001467537065836', '0', '1');
INSERT INTO `message` VALUES ('18', '18', '23', '0', '新的每日订单:2016-07-03', '0000000000000000000000000000000000000000000000000001467537381082', '0', '1');
INSERT INTO `message` VALUES ('19', '18', '24', '0', '新的每日订单:2016-07-03', '0000000000000000000000000000000000000000000000000001467537381098', '0', '1');
INSERT INTO `message` VALUES ('20', '18', '25', '0', '新的每日订单:2016-07-03', '0000000000000000000000000000000000000000000000000001467537381111', '0', '1');
INSERT INTO `message` VALUES ('21', '18', '26', '0', '新的每日订单:2016-07-04', '0000000000000000000000000000000000000000000000000001467559898854', '0', '1');
INSERT INTO `message` VALUES ('22', '18', '27', '0', '新的每日订单:2016-07-04', '0000000000000000000000000000000000000000000000000001467559898870', '0', '1');
INSERT INTO `message` VALUES ('23', '18', '28', '0', '新的每日订单:2016-07-04', '0000000000000000000000000000000000000000000000000001467559898886', '0', '1');

-- ----------------------------
-- Table structure for ordergoods
-- ----------------------------
DROP TABLE IF EXISTS `ordergoods`;
CREATE TABLE `ordergoods` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `forderid` int(32) DEFAULT NULL,
  `fgoodsid` int(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orderuserid` (`forderid`),
  KEY `fgoodsid` (`fgoodsid`),
  CONSTRAINT `fgoodsid` FOREIGN KEY (`fgoodsid`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ordergoods_ibfk_1` FOREIGN KEY (`forderid`) REFERENCES `userorder` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ordergoods
-- ----------------------------
INSERT INTO `ordergoods` VALUES ('5', '19', '14');
INSERT INTO `ordergoods` VALUES ('6', '20', '11');
INSERT INTO `ordergoods` VALUES ('7', '20', '12');
INSERT INTO `ordergoods` VALUES ('8', '20', '13');
INSERT INTO `ordergoods` VALUES ('9', '26', '6');
INSERT INTO `ordergoods` VALUES ('10', '27', '7');
INSERT INTO `ordergoods` VALUES ('11', '28', '8');

-- ----------------------------
-- Table structure for payment
-- ----------------------------
DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `orderids` varchar(255) CHARACTER SET utf8 NOT NULL,
  `fuserid` int(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `ispay` int(32) DEFAULT NULL,
  `paytype` int(32) DEFAULT NULL,
  `totalprice` int(32) DEFAULT NULL,
  `payinfo` longtext CHARACTER SET utf8,
  `callback` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `thirdcallbackinfo` longtext CHARACTER SET utf8 COMMENT '第三方服务器的回调的信息.通常只保存成功的.',
  PRIMARY KEY (`id`),
  KEY `orderuserid` (`fuserid`),
  KEY `forderid` (`orderids`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of payment
-- ----------------------------
INSERT INTO `payment` VALUES ('1', '23-24-25', '18', '2016-07-03 00:00:00', '2016-07-03 00:00:00', '2', '0', '0', 'BA2905F1E0F629098BB0E0B03C0F4DFF', 'http://shuiguorili.com/payCallBack?data=BA2905F1E0F629098BB0E0B03C0F4DFF', null);
INSERT INTO `payment` VALUES ('2', '26-27-28', '18', '2016-07-03 00:00:00', '2016-07-03 00:00:00', '2', '0', '600', 'C06C88F4FD7F19F426857CA4CDBA4FF2E6E685BE3D0D0B94D96FD000607681109474AE73EA98EA173101B22F525AE1B1', 'http://shuiguorili.com/payCallBack?data=C06C88F4FD7F19F426857CA4CDBA4FF2E6E685BE3D0D0B94D96FD000607681109474AE73EA98EA173101B22F525AE1B1', null);
INSERT INTO `payment` VALUES ('3', '21', '18', '2016-07-20 00:00:00', '2016-07-20 00:00:00', '2', '0', '0', 'partner=\"2088502990467974\"&seller_id=\"1031320252@qq.com\"&out_trade_no=\"0720003835-7681\"&subject=\"水果日历商品\"&body=\"鲜美的水果\"&total_fee=\"0.0\"&notify_url=\"http://shuiguorili.com:8080/payCallBack?paymentid=3\"&service=\"mobile.securitypay.pay\"&payment_type=\"1\"&_input_charset=\"utf-8\"&it_b_pay=\"30m\"&return_url=\"m.alipay.com\"&sign=\"N7oiXJr8t4jBcWMvtAHEnYoTZVYQomVou3G0DAmcAi5OQhdJzJx52IHvCwzc2qDKWBFzEZa67h%2FNHXuJbkjdJXzjJ2WpMaQeixV%2BYFkB%2FFbnL1ForEUbP5k6KLu5I5lbXpU8JYh9jojkFrxCy62MyBCmP1uOtO7S2SZifPikyEw%3D\"&sign_type=\"RSA\"', 'http://shuiguorili.com:8080/payCallBack?paymentid=3', null);

-- ----------------------------
-- Table structure for place
-- ----------------------------
DROP TABLE IF EXISTS `place`;
CREATE TABLE `place` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `fuserid` int(32) DEFAULT NULL,
  `cityid` int(32) unsigned zerofill DEFAULT NULL,
  `areaid` int(32) unsigned zerofill DEFAULT NULL,
  `provinceid` int(32) unsigned zerofill DEFAULT NULL,
  `place` varchar(1024) NOT NULL,
  `placedetail` varchar(1024) NOT NULL,
  `latitude` varchar(1024) DEFAULT NULL,
  `longitude` varchar(1024) DEFAULT NULL,
  `recvusername` varchar(1024) NOT NULL,
  `recvusertel` varchar(1024) NOT NULL,
  `isdefault` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fuserid` (`fuserid`),
  CONSTRAINT `place_ibfk_1` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of place
-- ----------------------------
INSERT INTO `place` VALUES ('1', '18', '00000000000000000000000000000000', '00000000000000000000000000000000', '00000000000000000000000000000000', '321湖北省武汉市洪山区', 'xx大厦1-101', '0', '0', '0', '0', '0');
INSERT INTO `place` VALUES ('3', '18', '00000000000000000000000000000000', '00000000000000000000000000000000', '00000000000000000000000000000000', '湖北省武汉市洪山区', 'xx大厦1-101', '0', '0', '0', '0', '1');
INSERT INTO `place` VALUES ('4', '18', '00000000000000000000000000000000', '00000000000000000000000000000000', '00000000000000000000000000000000', '湖北省武汉市洪山区', 'xx大厦1-101', '0', '0', 'test', '123456789', '0');

-- ----------------------------
-- Table structure for recommend
-- ----------------------------
DROP TABLE IF EXISTS `recommend`;
CREATE TABLE `recommend` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `bannerimgurl` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `isalive` int(11) DEFAULT NULL,
  `contexturl` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `fgoodstype` int(32) DEFAULT NULL,
  `fgoodsid` int(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fgoodstype` (`fgoodstype`),
  KEY `fgoodsid` (`fgoodsid`),
  CONSTRAINT `recommend_ibfk_1` FOREIGN KEY (`fgoodstype`) REFERENCES `goodstype` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `recommend_ibfk_2` FOREIGN KEY (`fgoodsid`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of recommend
-- ----------------------------
INSERT INTO `recommend` VALUES ('4', '4', '1', '2016-05-17 22:17:12', '2016-05-27 22:17:18', '0', 'dboomsky.com/index.html', '5', '1');
INSERT INTO `recommend` VALUES ('5', '5', '1', '2016-05-17 22:17:12', '2016-05-27 22:17:18', '0', 'dboomsky.com/index.html', '6', '2');
INSERT INTO `recommend` VALUES ('6', '6', '1', '2016-05-17 22:17:12', '2016-05-27 22:17:18', '0', 'dboomsky.com/index.html', '6', '3');
INSERT INTO `recommend` VALUES ('7', '7', '1', '2016-05-17 22:17:12', '2016-05-27 22:17:18', '0', 'dboomsky.com/index.html', '6', '4');
INSERT INTO `recommend` VALUES ('8', '8', '1', '2016-05-17 22:17:12', '2016-05-27 22:17:18', '0', 'dboomsky.com/index.html', '6', '5');
INSERT INTO `recommend` VALUES ('9', '9', '1', '2016-05-17 22:17:12', '2016-05-27 22:17:18', '0', 'dboomsky.com/index.html', '7', '6');

-- ----------------------------
-- Table structure for shoppingcar
-- ----------------------------
DROP TABLE IF EXISTS `shoppingcar`;
CREATE TABLE `shoppingcar` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `fuserid` int(32) NOT NULL,
  `fgoodsid` int(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shoppingoreruserid` (`fuserid`),
  KEY `shoppingorergoodsid` (`fgoodsid`),
  CONSTRAINT `shoppingorergoodsid` FOREIGN KEY (`fgoodsid`) REFERENCES `goods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shoppingoreruserid` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of shoppingcar
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `username` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `token` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(1024) NOT NULL,
  `paypassword` varchar(1024) DEFAULT NULL,
  `nickname` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `tel` varchar(1024) DEFAULT NULL,
  `avatar` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `avatarhd` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `memberpoints` int(32) DEFAULT NULL,
  `memberpointsbalance` int(32) DEFAULT NULL,
  `banlance` int(32) DEFAULT NULL,
  `sacncode` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `sacncodeimage` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `thirdid` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `thirdtoken` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `thirdnickname` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `thirdtype` int(32) DEFAULT NULL,
  `imappkey` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `imusername` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  `impassword` varchar(1024) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('18', 'test', 'df71df92c31111f810a7d89bd2c2e35d', '029C5A3E072B8FD7C88E048F1ADA20D5556A3DF55B02610495EBAEE6ED321F945FA9D6A4655C781A', '', '王岩2', '', '\\upload\\512x.png', '\\upload\\512x.png', '0', '0', '0', '/scancode', '/scancode/img', '0', '', '', '-1', 'geliang0120#shuiguorili', 'test', '73d9dc83f828df2cab62680bd1522543');
INSERT INTO `user` VALUES ('19', 'test1', 'b42a86db0221aa3e38893aa52675407b', 'A7F259A1C5823B99C6C76FD0985E95703CAA034DA576EB73EDD90E8C3E48EDA25FA9D6A4655C781A', '', 'test1', '', '/img/defaultavatar.jpg', '/img/defaultavatar.jpg', '0', '0', '0', '/scancode', '/scancode/img', '0', '', '', '-1', 'geliang0120#shuiguorili', 'test1', '6aace82ef4b947829daa11a61acb7052');
INSERT INTO `user` VALUES ('29', 'qq_8F7C4A23797BBC08115E5F0842A24B4C', '3a5505895917340a93cb86e6fada612a', '0FDD052DF457FDCD1B454466688F2F8BF2475A4C16309A80FC3258E2807816015FA9D6A4655C781A', '', 'qq_8F7C4A23797BBC08115E5F0842A24B4C', '', '/img/defaultavatar.jpg', '/img/defaultavatar.jpg', '0', '0', '0', '/scancode', '/scancode/img', '8F7C4A23797BBC08115E5F0842A24B4C', 'E6ACDCBE6E8B3B48201B743F21A9051A', '', '1', 'geliang0120#shuiguorili', 'qq_8F7C4A23797BBC08115E5F0842A24B4C', 'ea6be77277bbbda854aa9e1a4410395e');

-- ----------------------------
-- Table structure for userorder
-- ----------------------------
DROP TABLE IF EXISTS `userorder`;
CREATE TABLE `userorder` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `ordertype` int(32) DEFAULT NULL COMMENT '订单方式,0为普通订单,1 为默认订单',
  `fuserid` int(32) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL COMMENT '订单开始创建的时间',
  `endtime` datetime DEFAULT NULL COMMENT '订单到期结束的时间',
  `ispay` int(32) DEFAULT NULL,
  `paytype` int(32) DEFAULT NULL,
  `totalprice` int(32) DEFAULT NULL,
  `placeid` int(32) DEFAULT NULL,
  `sendername` varchar(255) DEFAULT '',
  `senderphone` varchar(255) DEFAULT '',
  `postmanname` varchar(255) DEFAULT '',
  `postmanphone` varchar(255) DEFAULT NULL,
  `dispatchtime` datetime DEFAULT NULL COMMENT '订单配送时间',
  PRIMARY KEY (`id`),
  KEY `fuserid` (`fuserid`) USING BTREE,
  KEY `placeid` (`placeid`),
  CONSTRAINT `fuserid` FOREIGN KEY (`fuserid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userorder_ibfk_1` FOREIGN KEY (`placeid`) REFERENCES `place` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userorder
-- ----------------------------
INSERT INTO `userorder` VALUES ('19', '0', '18', '2016-06-21 00:00:00', '2016-06-21 00:00:00', '0', '0', '28000', '1', 'admin', '', '', '', null);
INSERT INTO `userorder` VALUES ('20', '0', '18', '2016-06-21 00:00:00', '2016-06-21 00:00:00', '0', '0', '64000', '1', 'admin', '', '', '', null);
INSERT INTO `userorder` VALUES ('21', '0', '18', '2016-07-03 00:00:00', '2016-07-03 00:00:00', '0', '0', '0', '1', 'admin', '', '', '', null);
INSERT INTO `userorder` VALUES ('22', '0', '18', '2016-07-03 00:00:00', '2016-07-03 00:00:00', '0', '0', '0', '1', 'admin', '', '', '', null);
INSERT INTO `userorder` VALUES ('26', '1', '18', '2016-07-03 00:00:00', '2016-07-04 00:00:00', '0', '0', '300', '1', 'admin', '', '', '', '2016-07-03 00:00:00');
INSERT INTO `userorder` VALUES ('27', '1', '18', '2016-07-03 00:00:00', '2016-07-04 00:00:00', '0', '0', '200', '1', 'admin', '', '', '', '2016-07-04 00:00:00');
INSERT INTO `userorder` VALUES ('28', '1', '18', '2016-07-03 00:00:00', '2016-07-04 00:00:00', '0', '0', '100', '1', 'admin', '', '', '', '2016-07-05 00:00:00');
