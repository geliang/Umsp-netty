package gl.java.utils;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.chanceit.framework.utils.encode.Des;



public class DES {
	private static final String DES_KEY = "geliang0120";
	// 算法名称
	public static final String DES_ALGORITHM = "DES";
	// 算法名称/加密模式/填充方式
	public static final String CIPHER_ALGORITHM_ECB = "DES/ECB/PKCS5Padding";
	public static final String CIPHER_ALGORITHM_CBC = "DES/CBC/PKCS5Padding";

	/**
	 * DES加密
	 * 
	 * @param plainData
	 * @param secretKey
	 * @return
	 * @throws Exception
	 */
	public static String encryption(String plainData, String secretKey)
			throws Exception {

		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance(DES_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, generateKey(secretKey));

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {

		}

		try {
			// 为了防止解密时报javax.crypto.IllegalBlockSizeException: Input length must
			// be multiple of 8 when decrypting with padded cipher异常，
			// 不能把加密后的字节数组直接转换成字符串
			byte[] buf = cipher.doFinal(plainData.getBytes());

			return Base64Utils.encode(buf);

		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			throw new Exception("IllegalBlockSizeException", e);
		} catch (BadPaddingException e) {
			e.printStackTrace();
			throw new Exception("BadPaddingException", e);
		}

	}

	/**
	 * DES解密
	 * 
	 * @param secretData
	 * @param secretKey
	 * @return
	 * @throws Exception
	 */
	public static String decryption(String secretData, String secretKey)
			throws Exception {

		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance(DES_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, generateKey(secretKey));

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new Exception("NoSuchAlgorithmException", e);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			throw new Exception("NoSuchPaddingException", e);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			throw new Exception("InvalidKeyException", e);

		}

		try {

			byte[] buf = cipher.doFinal(Base64Utils.decode(secretData
					.toCharArray()));

			return new String(buf);

		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			throw new Exception("IllegalBlockSizeException", e);
		} catch (BadPaddingException e) {
			e.printStackTrace();
			throw new Exception("BadPaddingException", e);
		}
	}

	/**
	 * 获得秘密密钥
	 * 
	 * @param secretKey
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws InvalidKeyException
	 */
	private static SecretKey generateKey(String secretKey)
			throws NoSuchAlgorithmException, InvalidKeySpecException,
			InvalidKeyException {
		SecretKeyFactory keyFactory = SecretKeyFactory
				.getInstance(DES_ALGORITHM);
		DESKeySpec keySpec = new DESKeySpec(secretKey.getBytes());
		keyFactory.generateSecret(keySpec);
		return keyFactory.generateSecret(keySpec);

	}

	// public static void main(String[] a) throws Exception{
	// String input = "cy11Xlbrmzyh:604:301:1353064296";
	// String key = "37d5aed075525d4fa0fe635231cba447";
	//
	// DES des = new DES();
	//
	// String result = des.encryption(input, key);
	// System.out.println(result);
	//
	// System.out.println(des.decryption(result, key));
	//
	// }

	static class Base64Utils {

		static private char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
				.toCharArray();
		static private byte[] codes = new byte[256];
		static {
			for (int i = 0; i < 256; i++)
				codes[i] = -1;
			for (int i = 'A'; i <= 'Z'; i++)
				codes[i] = (byte) (i - 'A');
			for (int i = 'a'; i <= 'z'; i++)
				codes[i] = (byte) (26 + i - 'a');
			for (int i = '0'; i <= '9'; i++)
				codes[i] = (byte) (52 + i - '0');
			codes['+'] = 62;
			codes['/'] = 63;
		}

		/**
		 * 将原始数据编码为base64编码
		 */
		static public String encode(byte[] data) {
			char[] out = new char[((data.length + 2) / 3) * 4];
			for (int i = 0, index = 0; i < data.length; i += 3, index += 4) {
				boolean quad = false;
				boolean trip = false;
				int val = (0xFF & (int) data[i]);
				val <<= 8;
				if ((i + 1) < data.length) {
					val |= (0xFF & (int) data[i + 1]);
					trip = true;
				}
				val <<= 8;
				if ((i + 2) < data.length) {
					val |= (0xFF & (int) data[i + 2]);
					quad = true;
				}
				out[index + 3] = alphabet[(quad ? (val & 0x3F) : 64)];
				val >>= 6;
				out[index + 2] = alphabet[(trip ? (val & 0x3F) : 64)];
				val >>= 6;
				out[index + 1] = alphabet[val & 0x3F];
				val >>= 6;
				out[index + 0] = alphabet[val & 0x3F];
			}

			return new String(out);
		}

		/**
		 * 将base64编码的数据解码成原始数据
		 */
		static public byte[] decode(char[] data) {
			int len = ((data.length + 3) / 4) * 3;
			if (data.length > 0 && data[data.length - 1] == '=')
				--len;
			if (data.length > 1 && data[data.length - 2] == '=')
				--len;
			byte[] out = new byte[len];
			int shift = 0;
			int accum = 0;
			int index = 0;
			for (int ix = 0; ix < data.length; ix++) {
				int value = codes[data[ix] & 0xFF];
				if (value >= 0) {
					accum <<= 6;
					shift += 6;
					accum |= value;
					if (shift >= 8) {
						shift -= 8;
						out[index++] = (byte) ((accum >> shift) & 0xff);
					}
				}
			}
			if (index != out.length)
				throw new Error("miscalculated data length!");
			return out;
		}
	}

	public static String enCrypto(String string) {
		try {
			return encryption(string, DES_KEY);
		} catch (Exception e) {
			e.printStackTrace();
			return string;
		}
	}
	public static void main(String[] args) throws Exception {
		String s = Des.deCrypto("3EB2E7E1E071DA90D1DE3D472C78DC0291E06C4CAB2B649A9A47DC5AC97DCADD7587EA5BF4DC9B36A45874A9443211262B38F67A774B0A1B6A1F48710835663553E1D4AFC0010F044F6DF6AEFFAA59FEB693CE79A989E8AC63BC601396EB38364381E6C3001AED19CEC85908A911FB2BEED23565D1BD4EC20F907991CC08D7943F12CE498D9F103C17AE482E489E69EB02E88CBB418A165313C65351954F090C8F85162D0F180D51C2F67AA26697C2E5C3B3057688178ECAB36126EA91182486CC1D815967F7D757FBABE1305D295667AF4D902654B561987B86C1AF12808546AAE574E54B2AC1A3925D5AF9043B5710020F032C737D2DA477B235ADDB803F90DBF20D953072D0A9415577A55C7B39EC01241EF4149A31B37507F4BF0402B40DBF8C0F2D97B8E97DB5FE0C0344A0277F329BFF4E59A496910D4E97FA9A2DAEF36AEA191199011F465AD3F73B0D732FB87A9E8E58A02A8BAC7491366884A6AA9B6B0A63441E34155782E05A719F04AC7B8E2E16CB8E40F8B36AF42BCD18762BEBE6E98AED7E19BF41AC59CD9444189AB7A0F86862057425306ABFB219ACFC6520D9ABBE4B8F8876CB84022972AA3B3754A3173A31E55A3DF69F489FC0ACA6346D4ABB89B5C87E2002D96390A27038C26243555BDB5448706306324BA7ADDBAE54FB933E94C31540468CAAA9DD5B87EBE8C0522CD5E0B08660B7E15356D6B410AD2E00EAFCCD741F6D8534D3F0B11381BA2CEFD7AF4DA558B20199F130FD74CFFF8DEAD6BFC160D151", "xusanduo");
		System.out.println(s);
	}
}
