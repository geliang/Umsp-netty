package gl.java.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileUtils {

	public static void mvFile(File fileByUpload, String dest) throws IOException {
		Files.copy(fileByUpload.toPath(), new File(dest).toPath());
		fileByUpload.delete();
	}

	public static void copyFile(File fileByUpload, String dest)
			throws IOException {
		File file = new File(dest);
		if (file.exists()) {
			file.delete();
		}
		Files.copy(fileByUpload.toPath(), file.toPath());
	}

}
