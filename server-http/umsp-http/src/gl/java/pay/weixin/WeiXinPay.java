package gl.java.pay.weixin;

import gl.java.pay.IPay;
import gl.java.utils.MD5;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.SAXException;

@SuppressWarnings("deprecation")
public class WeiXinPay implements IPay {
	public static final String appId = "wx089980fb6a9ba862";
	public static final String partnerId = "1309908201";
	public static final String packageValue = "Sign=WXPay";
	public static final String nonceStr = "1101000000140429eb40476f8896f4c9";
	public static final String appKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASC";
	public static final String clientIP = "0.0.0.0";

	@Override
	/**
	 * price 单位是分
	 */
	public String genPayInfo(String orderID, String subject, String body,
			String price, String callback) {
		try {
			String nonce_str = System.currentTimeMillis() + "";
			String stringA = "appid=" + appId + "&body=" + subject + "&mch_id="
					+ partnerId + "&nonce_str=" + nonce_str + "&notify_url="
					+ callback + "&out_trade_no=" + orderID
					+ "&spbill_create_ip=" + clientIP + "&total_fee=" + price
					+ "&trade_type=APP";
			final String temp = stringA + "&key=" + appKey;
//			System.out.println(temp);
			String sign = MD5.md5(temp).toUpperCase();

			String postString = "" + "<xml>"
					+ "<appid>wx089980fb6a9ba862</appid>" + "<body>"
					+ subject
					+ "</body>"
					+ "<mch_id>"
					+ partnerId
					+ "</mch_id>"
					+ "<nonce_str>"
					+ nonce_str
					+ "</nonce_str>"
					+ "<notify_url>"
					+ callback
					+ "</notify_url>"
					+ "<out_trade_no>"
					+ orderID
					+ "</out_trade_no>"
					+ "<sign>"
					+ sign
					+ "</sign>"
					+ "<spbill_create_ip>"
					+ clientIP
					+ "</spbill_create_ip>"
					+ "<total_fee>"
					+ price
					+ "</total_fee>"
					+ "<trade_type>APP</trade_type>"
					+ "</xml>";

			final String url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
			@SuppressWarnings("resource")
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			StringEntity postingString = new StringEntity(postString);// json传递
			post.setEntity(postingString);
			// post.setHeader();
			post.setHeader("Content-type", "application/json");
			HttpResponse response = httpClient.execute(post);
			String content;

			content = EntityUtils.toString(response.getEntity());

//			System.out.println(content);
			String prepayid;
			// <prepay_id><![CDATA[wx20160726003553b6926c9f940430045912]]></prepay_id>
			Pattern p = Pattern
					.compile("<prepay_id><!\\[CDATA\\[(.*?)\\]\\]><\\/prepay_id>");
			Matcher m = p.matcher(content);
			while (m.find()) {
				prepayid = m.group(1);
				return prepayid;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return "";
	}

	public static void main(String[] args) throws HttpException, IOException,
			ParserConfigurationException, SAXException {
		final String genPayInfo = new WeiXinPay().genPayInfo("11", "sword",
				"one ", "22", "http://fas.com");
		System.out.println(genPayInfo);

	}

}
