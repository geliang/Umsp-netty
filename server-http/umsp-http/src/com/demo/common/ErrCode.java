package com.demo.common;


public enum ErrCode {
    IllegalParameter(201,"IllegalParameter"),
    UserNotExist(202,"UserNotExist");
    public int errCode;
    public String describe;
    private ErrCode(int errCode,String describe) {
        this.errCode = errCode;
        this.describe = describe;
    }
}
