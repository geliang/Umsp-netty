package com.demo.controls;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.demo.common.model.Recommend;
import com.jfinal.kit.JsonKit;

/**
 * IndexController
 */
public class RecommendController extends BaseController {
	/**
	 * 活动推荐,在单独的recommand表中,以提高性能
	 */
	public void index() {
		Map<String,String[]> map = getParaMap();
		if(map.size()<=0){
			renderText("what do you want to recommend?");
			return;
		}
//		List<Goods> users = Goods.dao.find("select * from goods where fgoodstype = ? order by id desc",getPara("type"));
		List<Recommend> users = Recommend.dao.find("select * from recommend where fgoodstype = ? order by id desc",getPara("type"));
		JSONArray ja = JSONArray.parseArray(JsonKit.toJson(users));
		for (int i = 0; i < ja.size(); i++) {
			JSONObject jsonObject = ja.getJSONObject(i);
			jsonObject.put("goods",getGoods(jsonObject.getIntValue("fgoodsid")));
		}
		renderJson(ja.toString());
	}

}