package com.demo.controls;

import java.util.List;

import com.demo.common.model.Activity;

/**
 * IndexController
 */
public class ActivityController extends BaseController {
	public void index() {
		List<Activity> users = Activity.dao.find("select * from activity where isvalue = ? ",0);
		renderJson(users);
	}
}