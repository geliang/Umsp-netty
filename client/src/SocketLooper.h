/*
 * GWSocketHandler.h
 *
 *  Created on: 2016-5-4
 *      Author: Administrator
 */

#ifndef SOCKETLOOPER_H_
#define SOCKETLOOPER_H_

#include <iostream>
using namespace std;

#include "common.h"
#include "SocketClient.h"
#include "GWSocketHandler.h"
#include <stdlib.h>
#include <string.h>
#include "SyncQueue.h"
#include "Thread.h"
class SocketLooper : public Thread{
public:
	SocketLooper(const char* host,int port,int timeoutInSecond,int loopTag);
	virtual ~SocketLooper();
	void startLoop();
	void stopLoop();
	void setRecvHandler(handlePacket handler,handleSocketClosed handler1);
	int putToSend(char* data,int size);
private:
	virtual void run() {
		loop();
	}

	void loop();
	SyncQueue<Packet*>  sendQueue;
	volatile bool isStop;
	volatile bool isLooping;
	handlePacket handlePacketImp;
	handleSocketClosed handleSocketClosedImp;
	SocketClient* client;
	int looperTag;
	int mTimeout;
	const static int LOOP_INTERVALS = 16;
};

#endif /* SOCKETLOOPER_H_ */
