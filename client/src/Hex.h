#ifndef _HEX_H_
#define _HEX_H_
#ifdef __cplusplus
extern "C" {
#endif
void bytesToString(char* src, int srcLen, char*buf, int outLen) ;

void stringToBytes(char* src, int srcLen, char* outArray, int outLen);

#ifdef __cplusplus
}
#endif
#endif /* _HEX_H_ */

//void testHex() {
//    int srcSize = 1024;
//    int detSize = srcSize * 2;
//    char src[1024] = {};
//    char outSrc[2048] = {};
//    memset(outSrc, 0, detSize);
//    //struct timeval start, end;
//    //gettimeofday(&start, NULL);
//    for (int i = 0; i < srcSize; i++) {
//        sprintf(outSrc, "%s%02x", outSrc, (unsigned char)(src[i]));
//    }
//    //gettimeofday(&end, NULL);
//    //gettimeofday(&start, NULL);
//    //int timeuse = 1000000 * (end.tv_sec - start.tv_sec) + end.tv_usec
//    //		- start.tv_usec;
//    //printf("time: %d us\n", timeuse);
//    //gettimeofday(&end, NULL);
//    bytesToString(src, srcSize, outSrc, detSize);
//    //timeuse = 1000000 * (end.tv_sec - start.tv_sec) + end.tv_usec
//    //		- start.tv_usec;
//    //printf("time: %d us\n", timeuse);
//    //printf("%s", outSrc);
//    //LOGI("%s", outSrc);
//    char outSrc1[4];
//    memset(outSrc1, 0, 4);
//    stringToBytes(outSrc, 8, outSrc1, 4);
//    for (int i = 0; i < 4; i++) {
//        LOGI("%02x", outSrc1[i] & 0xFF);
//    }
//}