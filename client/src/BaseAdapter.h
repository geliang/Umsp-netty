/*
 * BaseAdapter.h
 *
 *  Created on: 2016-6-29
 *      Author: Administrator
 */

#ifndef BASEADAPTER_H_
#define BASEADAPTER_H_

#include "common.h"
#include "cJSON.h"
#include "cJSON_Utils.h"
extern "C" {
#define  JSON_BRIDGE_METHID "action"
#define  JSON_BRIDGE_PARAMS "params"
#define  JSON_BRIDGE_BYTES  "bytes"
#pragma pack(1)
	typedef int (*handlePacket)(Packet* packet);
	typedef int (*handleSocketClosed)(int tag);
	typedef  handlePacket handleJson ;
	typedef int (*MethodBridge)(int methodID,char* parameters, int parametersSize, char* byte,int byteSize,void *client );
#pragma pack()


EXPORT void setMethodBridge(MethodBridge binder);

EXPORT int invokeMethod(char* jsonString, int jsonStringSize,void *client );

EXPORT int handleJsonPacket(handleJson handler);
EXPORT int pushMsgToRecvQueue(Packet* packet);

}

#endif /* BASEADAPTER_H_ */
