#ifndef SOCKETCHANNEL_H_
#define SOCKETCHANNEL_H_

#ifdef _WIN32
#include <WinSock2.h>
#include <time.h>
#define _TIMESPEC_DEFINED
#define MSG_DONTWAIT 0
#else
#include <sys/socket.h>
#endif // _WIN32

#include <string.h>
#include <errno.h>
#include "utils.h"

//封装socket 屏蔽平台区别
class SocketChannel {
public:
	SocketChannel();
	virtual ~SocketChannel();
public:
	//返回socketfd,<=0则为失败
	int connectChannel(int isTcp,const char* host,int port,int recvTimeout);
	void close();
	//返回接收到的字节数,<=0则为失败
	int recvData(char* buf,int size);
	//返回Recv Buffer总存在的数据长度;
	int peek(char* buf, int maxSize);

	//返回发送完成的字节数,<=0则为失败
	int sendData(const char* buf,int size);

public:
	int fd;
	int socketType;
private:
	Buf mRecvBuf;
};


#endif /* SOCKETCHANNEL_H_ */
