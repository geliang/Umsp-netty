#ifndef _H_MS_MUTEX_RW_LOCKER_H_
#define _H_MS_MUTEX_RW_LOCKER_H_


#if defined(_WIN32)
#include "Windows.h"
#else
#include <pthread.h>
#endif//


class MsMutexLocker
{
public:
	MsMutexLocker()
	{
#if defined(_WIN32)
		::InitializeCriticalSection(&m_objMutex);
#else
		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
#ifndef __CYGWIN__
		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
#endif
		pthread_mutex_init(&m_objMutex, &attr);//
		pthread_mutexattr_destroy(&attr);

#endif//
	};

	~MsMutexLocker()
	{
#ifdef _WIN32
		::DeleteCriticalSection(&m_objMutex);
#else
		pthread_mutex_destroy(&m_objMutex);
#endif
	};

	void lock()
	{
#if defined(_WIN32)
		::EnterCriticalSection(&m_objMutex);
#else
		pthread_mutex_lock(&m_objMutex);
#endif
	};
	void unlock()
	{
#ifdef _WIN32
		::LeaveCriticalSection(&m_objMutex);
#else
		pthread_mutex_unlock(&m_objMutex);
#endif
	};

private:
#if defined(_WIN32)
	CRITICAL_SECTION m_objMutex;
#else
	pthread_mutex_t  m_objMutex;
#endif
};

class MsAutoLock {
public:
	MsAutoLock(MsMutexLocker* locker)
	{
		m_Lock = locker;
		if (m_Lock)
		{
			m_Lock->lock();
		}
	};

	~MsAutoLock()
	{
		if (m_Lock)
		{
			m_Lock->unlock();
			m_Lock = 0;
		}
	};

private:
	MsMutexLocker* m_Lock;
};

#endif//_H_MS_MUTEX_RW_LOCKER_H_


